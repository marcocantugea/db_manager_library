<?php

/*******************************************************************************
 * dbManager                                                                    *
 *                                                                              *
 * Version: 1.0                                                                 *
 * Date:    2019-07-30                                                          *
 * configor:  Jesús Labra   
 * Updated Version : 1.1
 * @author Marco Cantu Gea <marco.cantu.gea@gmail.com.com>        
 * Updates:
 * - Create a builder pattern
 * - Sanitize the text and values
 * - Ability to input configuration on constructor
 * - FIX inner join statement
 *******************************************************************************/

class dbManager
{

    protected $table;           // Namet table 
    protected $primaryKey;      // primary key table
    protected $conn;            // Save conection with database
    protected $sql;             // Save the query prepared for execution
    protected $where = 0;       // Check count where
    protected $orWhere = 0;     // Check count orWhere
    private $configJsonfile ="config.json"; // config json son file 

    /*******************************************************************************
     *                               Public methods                                 *
     *******************************************************************************/

    /**
     * Class constructor
     *
     * @param string $table
     * @param string $primaryKey
     * @param string $externalConfig
     */
    function __construct(string $table, string $primaryKey,array $externalConfig=null)
    {
    
        $this->table = $table;
        $this->primaryKey = $primaryKey;
        if(!empty($externalConfig)){
            $this->setConnection($externalConfig);
        }else{
            $config=$this->loadConfig();
            $this->setConnection($config);
        }
        
    }

    /**
     * Load the server configuration
     *
     * @return array
     */
    public function loadConfig():array {

        $config=array();

        if(!file_exists($this->configJsonfile)){
            return $config;
        }

        $config = file_get_contents($this->configJsonfile);
        $config = json_decode($config, true);

        if(count($config)<=0){
            trigger_error("The config file is empty or  null", E_USER_ERROR);
            exit;
        }

        return $config;
    }


    /**
     * Create the connection object
     *
     * @param array $config
     * @return self
     */
    public function setConnection(array $config)
    {
        if (!isset($config) && !is_array($config)) {
            trigger_error("The expected parameter for the connection must be of type Array.", E_USER_ERROR);
            exit;
        }

        if (!isset($config['server']) && empty($config['server'])) {
            trigger_error("The server is undefined or empty.", E_USER_ERROR);
            exit;
        }

        if (!isset($config['db_name']) && empty($config['db_name']) ) {
            trigger_error("The name of the database is undefined or empty.", E_USER_ERROR);
            exit;
        }

        if (!isset($config['user']) && empty($config['user'])) {
            trigger_error("The name of the user is not defined or is empty.", E_USER_ERROR);
            exit;
        }

        if (!isset($config['pass']) && empty($config['pass'])) {
            trigger_error("TThe password is not configured", E_USER_ERROR);
            exit;
        }

        $conn = new mysqli($config['server'], $config['user'], $config['pass'], $config['db_name']);
        if ($conn->connect_error) {
            die('Error de conexión: ' . $conn->connect_error);
        } else {
            $this->conn= $conn;
        }

        return $this;
    }

    /**
     *  Create a SQL select stament
     *
     * @return self
     */
    public function select()
    {
        $arg = func_get_args();
        $this->sql = 'SELECT ';

        if (func_num_args() == 0) {
            $this->sql .= '* FROM ' . $this->table;
        } else {
            if (func_num_args() > 1) {

                //sanitiza los valores obtenidos
                $SanitizedValues= array();
                foreach($arg as $value){
                    $SanitizedValues []= $this->sanitizeText($value);
                }

                $this->sql .= implode(',', $SanitizedValues);

                $this->sql .= ' FROM ' . $this->table;
            } else {
                $this->sql .= $arg[0] . ' FROM ' . $this->table;;
            }
        }

        return $this;
    }

    /**
     * Create or agregate the SQL condition (where)
     *
     * @param string $column
     * @param string $condition
     * @param string|int $value
     * @return self
     */
    public function where(string $column,string $condition, $value)
    {
        //Sanitize the input variables
        $sanitizedColumn = $this->sanitizeText($column);
        $sanitizedCondition= $this->sanitizeText($condition);
        $sanitizedValue= $this->sanitizeText($value);

        if ($this->where > 0) {
            $this->sql .= ' AND ' .$this->table.".". $sanitizedColumn . ' ' . $sanitizedCondition . ' ' . $sanitizedValue;
        } else {
            $this->sql .= ' WHERE ' . $this->table.".". $sanitizedColumn . ' ' . $sanitizedCondition . ' ' . $sanitizedValue;
        }
        $this->where++;
        
        return $this;
    }

    /**
     * Create or agregate to sentence where  the OR condition
     *
     * @param string $column
     * @param string $condition
     * @param string|int $value
     * @return self
     */
    public function orWhere(string $column,string $condition, $value)
    {
        //Sanitize the input variables
        $sanitizedColumn = $this->sanitizeText($column);
        $sanitizedCondition= $this->sanitizeText($condition);
        $sanitizedValue= $this->sanitizeText($value);

        if ($this->where > 0) {
            $this->sql .= ' OR ' . $this->table.".".$sanitizedColumn . ' ' . $sanitizedCondition . ' ' . $sanitizedValue;
        } else {
            $this->sql .= ' WHERE ' . $this->table.".".$sanitizedColumn . ' ' . $sanitizedCondition . ' ' . $sanitizedValue;
        }
        $this->orWhere++;

        return $this;
    }

    /**
     * Agregate the join statement to the query
     *
     * @param string $foreingTable
     * @param string $foreingTableId
     * @param string $primaryTableKey
     * @param string $alias
     * @return self
     */
    public function join(string $foreingTable,string $foreingTableId,string $primaryTableKey,string $alias=null)
    {

        if(empty($foreingTable) || empty($foreingTableId) || empty($primaryTableKey)){
            trigger_error("The condition is empty.", E_USER_ERROR);
                exit;
        }

        //sanitize the input variables
        $sanitizeForeingTable = $this->sanitizeText($foreingTable);
        $sanitizeForeingTableId= $this->sanitizeText($foreingTableId);
        $sanitizeprimaryTableKey=$this->sanitizeText($primaryTableKey);

        //checks if the WHERE statement is set
        $strWherePos= strpos($this->sql,"WHERE");
        if($strWherePos===false){
            //if is not set, it will add the join
            $this->sql .= ' JOIN ' . $sanitizeForeingTable . ((!empty($alias)) ? " AS " . $alias : "" )
                            .' ON '.((!empty($alias)) ? $alias : $foreingTable ).".".$sanitizeForeingTableId . ' = ' . $this->table.".".$sanitizeprimaryTableKey;
        }else{
            //if is set it will split from the where statement, and it will add the join statment
            $initalSqlstmt= substr($this->sql,0,$strWherePos);
            $finalSqlstmt = substr($this->sql,$strWherePos,count(str_split($this->sql))-1);
            
            $this->sql = $initalSqlstmt;
            $this->sql .= ' JOIN ' . $sanitizeForeingTable . ((!empty($alias)) ? " AS " . $alias : "" )
                            .' ON '.((!empty($alias)) ? $alias : $foreingTable ).".".$sanitizeForeingTableId . ' = ' . $this->table.".".$sanitizeprimaryTableKey;
            $this->sql .= " ". $finalSqlstmt;
            
        }

        return $this;
    }


    /**
     * Execute the SQL stament and obtain array
     *
     * @return array
     */
    public function getArray() :array
    {
        $rows = array();
        $result = mysqli_query($this->conn, $this->sql);
        if ($result) {
            while ($row = mysqli_fetch_array($result)) {
                array_push($rows, $row);
            }
            $this->orWhere = 0;
            $this->where = 0;
        }
        return $rows;
    }

    /**
     * Execute the SQL stament 
     *
     * @return array
     */
    public function get() : array{
        return $this->getArray();
    }

    
    /**
     * Prepare insert statement
     *
     * @return self
     */
    public function insert()
    {
        $arg = func_get_args();
        $this->sql = 'INSERT INTO ' . $this->table . ' VALUES ';
        if (func_num_args() != 0) {

            //sanitize the input arguments
            $sanitizeArgs=array();
            foreach($arg as $value){
                $sanitizeArgs[]= $this->sanitizeText($value);
            }

            if (func_num_args() > 1) {
                $this->sql .= '(null,' . implode(',', $sanitizeArgs) . ')';
            } else {
                $this->sql .= '(null,' . $sanitizeArgs[0] . ')';
            }
        } else {
            trigger_error("At least you must assign a parameter to the function..", E_USER_ERROR);
            exit;
        }

        return $this;
    }

    /**
     * Make the SQL update statement
     *
     * @param int $id_reg
     * @param mixed|string|int $values
     * @return self
     */
    public function update(int $id_reg, $values)
    {
        if(count($values)>=0){
            trigger_error("At least one value must be set on the array", E_USER_ERROR);
            exit; 
        }

        $this->sql = 'UPDATE ' . $this->table . ' SET ';
        $index=0;

        foreach($values as $field=>$value){
            if($index>0){
                $this->sql .= ",";
            }

            //sanitize fiels and values
            $sanitizedField= $this->sanitizeText($field);
            $sanitizedValue = $this->sanitizeText($value);

            $this->sql .= $sanitizedField ."=" . $sanitizedValue;
            $index++;
        }
        
        $this->sql .=  ' WHERE ' . $this->primaryKey . ' = ' . $id_reg;

        return $this;
    }


    /**
     * Make the insert or update configurated
     *
     * @return array
     */
    public function save() :array
    {
        $data=array();
        $data['status'] = true;
        $data['msg'] = '';
        $result = $this->conn->query($this->sql);

        if (!$result){
            $data['status'] = false;
            $data['msg'] = "Error: " . $this->conn->error;
        }
        return $data;
    }

    /**
     * Prepare and excecute a delete statement
     *
     * @param integer $id_reg
     * @return void
     */
    public function delete(int $id_reg) : array
    {
        $data=array();
        $data['status'] = true;
        $data['msg'] = '';
        $this->sql = 'DELETE FROM ' . $this->table . ' WHERE ' . $this->primaryKey . ' = ' . $id_reg;
        $result = $this->conn->query($this->sql);

        if (!$result) {
            $data['status'] = false;
            $data['msg'] = "Error: " . $this->conn->error;
        }
        return $data;
    }

    /************************************
            Execute query personal
     *************************************/
    /**
     * Excecute custom query
     *
     * @param string $qry
     * @return void
     */
    public function qy(string $qry) : array
    {
        $rows = array();
        $this->sql = $qry;
        $result = $this->conn->query($this->sql);
        if ($result) {
            while ($row = mysqli_fetch_array($result)) {
                array_push($rows, $row);
            }
            $this->orWhere = 0;
            $this->where = 0;
        }
        return $rows;
    }

    private function sanitizeText(string $text){
        return mysqli_real_escape_string($this->conn,$text);
    }

    /**
     * Get the value of sql
     */ 
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * Set the value of configJsonfile
     *
     * @return  self
     */ 
    public function setConfigJsonfile($configJsonfile)
    {
        $this->configJsonfile = $configJsonfile;

        return $this;
    }
}
