<?php
echo '<center>******************</center>
      <br><center>Test dbManager</center><br>
      <center>Date: 2019-07-30</center><br>
      <center>Development: Jesús Labra</center><br>
      <center>*************************</center><br>
      <hr>';

/*******************************************************************************
* Test dbManager                                                               *
*                                                                              *
* Version: 1.0                                                                 *
* Date:    2019-07-30                                                          *
* Development:  Jesús Labra                                                       *
*******************************************************************************/

//Include library
include('dbml.php');
//Create a new object
$dbml = new dbManager('t_users','id_user');

/*******************************************************************************
*                              Test for insert                                 *
*******************************************************************************/
/*
//New insert in table t_users
$dbml->insert('\'Jesus\'','\'jlabraibarra@gmail.com\'',1);
//Execute insert (It returns an array with two data, 'status' of Boolean type and 
                  'msg' of string type, in case of error the status is false and 
                   brings the error message.)
print_r($dbml->save());
*/

/*******************************************************************************
*                              Test for Update                                 *
*******************************************************************************/
//Update table record (You need to send the data array to update and the id of the record)
/*
*   The array must have the syntax
*       $values = Array('column1'=>'value1','column2'=>'value2','column3'=>'value3'...);
*
*   The method must have the following syntax:
*       $dbml->update(value_primary_key,array_values);
*/
/*
$values = Array('name'=>'\'Jesus\'','email'=>'\'jesus@gmail.com\'','id_status'=>1);
$dbml->update(2,$values);
//We execute the update (It returns an array with two data, 'status' of Boolean type and 'msg' of string type, in case of error the status is false and brings the error message.).
echo('<pre>');
print_r($dbml->save());
echo('</pre>');
*/

/*******************************************************************************
*                              Test for Delete                                 *
*******************************************************************************/
/*
*   Delete the record from the table, the method takes as parameter an INT that 
*   is the id of the record.
*
*   In case of success or failure, an array with 'status' (true or false) and 
*   'msg' (depending on the status) returns.
*/
/*
echo('<pre>');
print_r($dbml->delete(1));
echo('</pre>');
*/

/*******************************************************************************
*                              Test for select                                 *
*******************************************************************************/
/*
*   The 'select' method prepares a query, you can send the table columns as a 
*   parameter like this:
*       select ('column1', 'column2' ...)
*                   or
*       select ('*'), by default brings all the columns.
*
*/
/*
$dbml->select();
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/

/*******************************************************************************
*                         Test for where (AND)                                 *
*******************************************************************************/
/*
*   Query where unique or concatenated with "and",  
*        for example: 
*                where column1 = 1 AND column2 = 1
*   
*   The method consists of three parameters: column, condition and value.
*        where($column,$condition,$value);
*/
/*
// Query for only where: SELECT * FROM t_users WHERE id_user = 1;
$dbml->select();
$dbml->where('id_user','=',1);
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');

// Query for mulyiple where: SELECT * FROM t_users WHERE id_user = 1 AND name = 'Jesus';
$dbml->select();
$dbml->where('id_user','=',1);
$dbml->where('name','=','\'Jesus\'');
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/


/*******************************************************************************
*                          Test for where (OR)                                 *
*******************************************************************************/
/*
*   Query where unique or concatenated with "OR",  
*        for example: 
*                where column1 = 1 OR column2 = 1
*   
*   The method consists of three parameters: column, condition and value.
*        where($column,$condition,$value);
*/
/*
// Query for only where: SELECT * FROM t_users WHERE id_user = 1;
$dbml->select();
$dbml->orWhere('id_user','=',1);
print_r($dbml->getArray());

// Query for mulyiple where: SELECT * FROM t_users WHERE id_user = 1 OR name = 'Jesus';
$dbml->select();
$dbml->orWhere('id_user','=',1);
$dbml->orWhere('name','=','\'Jesus\'');
print_r($dbml->getArray());
*/

/*******************************************************************************
*                          Test for QY                                         *
*******************************************************************************/
/*
*     This method is used to write queries directly
*     this method cannot be combined with the previous ones,
*     which has been added for the convenience of some queries 
*     that are not possible to carry out with the previous methods
*     below I show some examples.
*/

/*
$dbml->qy('SELECT name AS Nombre FROM t_users');
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/

/*
$dbml->qy('SELECT * FROM t_users');
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/

/*
$dbml->qy('SELECT * FROM t_users WHERE id_status = 1');
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/

/*
$dbml->qy('SELECT * FROM t_users WHERE id_status = 2');
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/