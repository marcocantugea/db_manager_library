<?php

include('../dbml.php');

if(isset($_POST['tabla']) && isset($_POST['idtabla'])){

    //Crea el objeto dbManajer
    //se puede obtener la configuracion del json(config.json) o definirla en el contructor
    
    //construye configuracion
    $config=["server"=>"localhost","db_name"=>"cunimed","user"=>"root","pass"=>""];
    $dbml = new dbManager($_POST['tabla'],$_POST['idtabla'],$config);

    //Prueba de SELECT
    //Select sencillo
    //Se puede utilizar metodos anidados    
    $selectResult = $dbml->select()->where($_POST['idtabla'], "=", 1)->get();
    echo "Select query :" . $dbml->getSql() . "<br>";

    //SELECT con campos
    $selectResultCampos= $dbml->select('count(1) as total')->where($_POST['idtabla'],">",0)->get();
    echo "Select query with fields :" . $dbml->getSql() . "<br>";

    //Select con varias condiciones
    $selectResultSomeWheres= $dbml->select($_POST['idtabla'])->where($_POST['idtabla'],">",0)->where($_POST['idtabla'],"<",10)->get();
    echo "Select query with some conditions :" . $dbml->getSql() . "<br>";

    //SELECT con orWhere
    $selectResultOrwhere = $dbml->select($_POST['idtabla'])->where($_POST['idtabla'],'=',0)->orWhere($_POST['idtabla'],"=",1)->get();
    echo "Select query with orWhere :" . $dbml->getSql() . "<br>";

    //Select con inner join
    if(isset($_POST['tabla2']) && isset($_POST['tablaid2'])){
        $selectResultInnerJoin= $dbml->select()
                                            ->join($_POST['tabla2'],$_POST['tablaid2'],$_POST['idtabla'])
                                            ->join($_POST['tabla'],$_POST['idtabla'],$_POST['idtabla'],"tabla2")
                                            ->where($_POST['idtabla'],'=',1)
                                            ->get();
        echo "Select query with join :" . $dbml->getSql() . "<br>";
    }

}



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo de dbml.php clase</title>
</head>
<body>

<h2>Ejemplo de dbml.php clase SELECTS</h2>

<form action="" method="post">
    <label for="tabla">Tabla: </label>
    <input type="text" name="tabla" id="tabla">
    <br>
    <br>
    <label for="idtabla">id de tabla: </label>
    <input type="text" name="idtabla" id="idtabla">
    <br>
    <br>
    <label for="tabla2">tabla inner join </label>
    <input type="text" name="tabla2" id="tabla2">
    <br>
    <br>
    <label for="tablaid2">foreing key id</label>
    <input type="text" name="tablaid2" id="tablaid2">
    <br>
    <br>
    <button type="submit">Probar</button>
</form>
<h1>Resultado query select</h1>
<?php

    if(!empty($selectResult)){
        foreach($selectResult as $row){
            foreach($row as $field=>$value){
                echo $field ."=". $value."<br>";
            }
        }
    }

?>
<br>
<br>
<h1>Resultado query select con campos</h1>
<?php
    if(!empty($selectResultCampos)){
        foreach($selectResultCampos as $row){
            foreach($row as $field=>$value){
                echo $field ."=". $value."<br>";
            }
        }
    }
?>

<br>
<br>
<h1>Resultado query select con varias condiciones</h1>
<?php
    if(!empty($selectResultSomeWheres)){
        foreach($selectResultSomeWheres as $row){
            foreach($row as $field=>$value){
                echo $field ."=". $value."<br>";
            }
        }
    }
?>

<br>
<br>
<h1>Resultado query select con orWhere</h1>
<?php
    if(!empty($selectResultOrwhere)){
        foreach($selectResultOrwhere as $row){
            foreach($row as $field=>$value){
                echo $field ."=". $value."<br>";
            }
        }
    }
?>

<br>
<br>
<h1>Resultado query select join</h1>
<?php
    if(!empty($selectResultInnerJoin)){
        foreach($selectResultInnerJoin as $row){
            foreach($row as $field=>$value){
                echo $field ."=". $value."<br>";
            }
        }
    }
?>

</body>
</html>